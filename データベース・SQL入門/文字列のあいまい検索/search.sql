1. nameが「マウスパッド」の商品データを取得する
select * from products
where
name = "マウスパッド";

2. nameに「パッド」の文字が含まれる商品データを取得する
select * from products
where
name LIKE '%パッド%'
and price >= 500;

3. nameに「パッド」の文字が含まれていない、かつpriceが500円以上の商品データを取得する
select * from products
where
name NOT LIKE '%パッド%'
and price >= 500;

