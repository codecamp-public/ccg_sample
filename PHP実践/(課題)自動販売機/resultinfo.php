<?php
// MySQL接続情報
$host     = 'localhost';
$username = 'root';   // MySQLのユーザ名
$password = 'root';   // MySQLのパスワード
$dbname   = 'camp';   // MySQLのDB名
$charset  = 'utf8';   // データベースの文字コード

// MySQL用のDNS文字列
$dsn = 'mysql:dbname='.$dbname.';host='.$host.';charset='.$charset;

$img_dir    = './img/';

$sql_kind   = '';
$result_msg = '';
$data    = array();
$err_msg = array();
$search_keyword    = "";
$search_sort       = "0";
$search_sort_create_datetime = 'desc';

if (isset($_POST['sql_kind']) === TRUE) {
  $sql_kind = $_POST['sql_kind'];
}

if ($sql_kind === 'search') {

  if (isset($_POST['search_keyword']) === TRUE) {
    $search_keyword = preg_replace('/\A[　\s]*|[　\s]*\z/u', '', $_POST['search_keyword']);
  }

  if (isset($_POST['search_sort']) === TRUE) {
    $search_sort = $_POST['search_sort'];
  }
}


try {
  // データベースに接続
  $dbh = new PDO($dsn, $username, $password);
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  try {
    // SQL文を作成
    $sql = 'SELECT drink_history.drink_id, drink_master.img, drink_master.drink_name, drink_master.price, drink_history.create_datetime
        FROM drink_history JOIN drink_master
        ON  drink_history.drink_id = drink_master.drink_id ';

    // 商品名で検索
    if($search_keyword !== '') {
      $sql = $sql . 'where drink_master.drink_name LIKE \'%' . $search_keyword . '%\'';
    }

    // ソート順
    if ($search_sort !== "0") {
      $search_sort_create_datetime = 'asc';
    }
    $sql = $sql . 'order by create_datetime ' . $search_sort_create_datetime;

    // SQL文を実行する準備
    $stmt = $dbh->prepare($sql);
    // SQLを実行
    $stmt->execute();
    // レコードの取得
    $rows = $stmt->fetchAll();
    // 1行ずつ結果を配列で取得します
    $i = 0;
    foreach ($rows as $row) {
      $data[$i]['img'] = htmlspecialchars($row['img'], ENT_QUOTES, 'UTF-8');
      $data[$i]['drink_name'] = htmlspecialchars($row['drink_name'], ENT_QUOTES, 'UTF-8');
      $data[$i]['price'] = htmlspecialchars($row['price'], ENT_QUOTES, 'UTF-8');
      $data[$i]['create_datetime'] = htmlspecialchars($row['create_datetime'],      ENT_QUOTES, 'UTF-8');

      $i++;
    }

  } catch (PDOException $e) {
    // 例外をスロー
    throw $e;
  }
} catch (PDOException $e) {
  $err_msg[] = '予期せぬエラーが発生しました。管理者へお問い合わせください。'.$e->getMessage();
}

?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>自動販売機</title>
  <style>
    section {
      margin-bottom: 20px;
      border-top: solid 1px;
    }

    table {
      width: 660px;
      border-collapse: collapse;
    }

    table, tr, th, td {
      border: solid 1px;
      padding: 10px;
      text-align: center;
    }

    caption {
      text-align: left;
    }

    .text_align_right {
      text-align: right;
    }

    .drink_name_width {
      width: 200px;
    }

    .input_text_width {
      width: 60px;
    }

    .status_false {
      background-color: #A9A9A9;
    }

    .drink_img_width {
      width: 50px;
    }
    .reesult_info {
      margin-top: 20px;
    }

  </style>
</head>
<body>
<?php if (empty($result_msg) !== TRUE) { ?>
  <p><?php print $result_msg; ?></p>
<?php } ?>
<?php foreach ($err_msg as $value) { ?>
  <p><?php print $value; ?></p>
<?php } ?>
  <h1>自動販売機管理ツール</h1>
  <section>
    <h2>購入履歴情報</h2>
    <form action="resultinfo.php" method="post" >
      <div><label>検索キーワード（商品名）: <input type="text" name="search_keyword" value="<?php $search_keyword !== '' ? print $search_keyword : ''; ?>"></label></div>
      <div>
        <label>並び順(購入日時):
          <select name="search_sort">
            <option value="0" <?php $search_sort === '0' ? print 'selected' : ''; ?> >降順</option>
            <option value="1" <?php $search_sort === '1' ? print 'selected' : ''; ?> >昇順</option>
          </select>
        </label>
      </div>
      <input type="hidden" name="sql_kind" value="search">
      <div>
        <input type="submit" value="　検索する　">
      </div>
    </form>
    <table class="reesult_info">
      <caption>購入履歴情報一覧</caption>
      <tr>
        <th>商品画像</th>
        <th>商品名</th>
        <th>価格</th>
        <th>購入日時</th>
      </tr>
<?php foreach ($data as $value)  { ?>
      <tr>
        <td><img class="drink_img_width" src="<?php print $img_dir . $value['img']; ?>"></td>
        <td class="drink_name_width"><?php print $value['drink_name']; ?></td>
        <td class="text_align_right"><?php print $value['price']; ?>円</td>
        <td class="text_align_right"><?php print $value['create_datetime']; ?></td>
      <tr>
<?php } ?>
    </table>
  </section>
</body>
</html>