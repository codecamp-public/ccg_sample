<?php
$rand = mt_rand(1, 6);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>if文課題</title>
</head>
<body>
    <h1>サイコロの数字：<?php print $rand; ?></h1>
<?php if($rand % 2 === 0){ ?>
    <p>サイコロの目は偶数です</p>
<?php } else { ?>
    <p>サイコロの目は奇数です</p>
<?php } ?>
</body>
</html>