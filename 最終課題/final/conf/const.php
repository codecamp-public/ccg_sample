<?php

// MySQL接続情報
$USER   = 'root';           // MySQLのユーザ名
$PASSWD = 'root';           // MySQLのパスワード
$DBNAME = 'camp';           // データベース名
$HOST   = 'localhost';      // データベースのホスト名又はIPアドレス
$CHARSET = 'utf8';          // データベースの文字コード
$DSN = 'mysql:dbname='.$DBNAME .';host='.$HOST.';charset='.$CHARSET;  // PDOのDSN

// 画像ファイルのディレクトリ
$IMG_DIR = './img_item/';

?>
